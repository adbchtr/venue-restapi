import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PlayerBooks extends BaseSchema {
  protected tableName = 'player_books'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.integer('book_id').unsigned().references('books.id')
      table.integer('user_id').unsigned().references('users.id')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
