/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.resource('venues','VenuesController').apiOnly().middleware({'*':['auth', 'verify', 'permission']})
Route.resource('fields','FieldsController').apiOnly().middleware({'index':['auth', 'verify'], 'store':['auth', 'verify'], 'update':['auth', 'verify'], 'destroy':['auth', 'verify']})
Route.post('/venues/:id/bookings', 'VenuesController.book').middleware(['auth', 'verify'])
Route.get('/bookings/:id', 'VenuesController.getbook').middleware(['auth', 'verify'])
Route.get('/bookings', 'VenuesController.allbook').middleware(['auth', 'verify'])
Route.post('/bookings/:id/join', 'VenuesController.addbook').middleware(['auth', 'verify'])
Route.delete('/bookings/:id/unjoin', 'VenuesController.deletebook').middleware(['auth', 'verify'])
Route.get('/bookingsbyuser', 'VenuesController.getbookbyuser')

Route.post('/verifikasi-otp', 'AuthController.otpConfirmation')

Route.post('/register', 'AuthController.register').as('auth.register')
Route.post('/login', 'AuthController.login').as('auth.login')