import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, HasMany, hasMany } from '@ioc:Adonis/Lucid/Orm'
import Venue from 'App/Models/Venue'
import Book from './Book'
export default class Field extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string
  
  @column()
  public type: string
  
  @column()
  public venueId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
  // @belongsTo(()=>User)
  // public author: BelongsTo<typeof User>
  @belongsTo(()=>Venue)
  public venues: BelongsTo<typeof Venue>
  // @hasMany(()=> Article)
  // public articles: HasMany<typeof Article>
  @hasMany(()=> Book)
  public bookings: HasMany<typeof Book>
}
