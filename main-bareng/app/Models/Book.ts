import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, HasMany, hasMany, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import PlayerBook from './PlayerBook'

export default class Book extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public fieldId: number

  @column()
  public playDateStart: DateTime

  @column()
  public playDateEnd: DateTime

  @column()
  public bookingUserId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
  // @belongsTo(()=>User)
  // public author: BelongsTo<typeof User>
  @belongsTo(()=>User,{
    foreignKey: 'bookingUserId'
  })
  public user: BelongsTo<typeof User>

  // @hasMany(()=>PlayerBook)
  // public players: HasMany<typeof PlayerBook>
  // @manyToMany(()=>PlayerBook)
  // public players: ManyToMany<typeof PlayerBook>
  @manyToMany(() => User, {
    pivotTable: 'player_books',
  })
  public players: ManyToMany<typeof User>
}
