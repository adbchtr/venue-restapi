import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
  column,
  beforeSave,
  BaseModel,
  hasMany,
  HasMany,
} from '@ioc:Adonis/Lucid/Orm'
import Book from 'App/Models/Book'
import PlayerBook from './PlayerBook'
/** 
 *  @swagger
 *  definitions:
 *   User:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       roleId:
 *         type: number
 *     required:
 *       - name
 *       - email
 *       - password
 *       - roleId
 */
/** 
 *  @swagger
 *  definitions:
 *   User_login:
 *     type: object
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 *     required:
 *       - email
 *       - password
 */
export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column()
  public isVerified: boolean
  
  @column()
  public roleId: number
  
  @column()
  public loggedIn: boolean
  
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  // @hasMany(()=> Article)
  // public articles: HasMany<typeof Article>
  @hasMany(()=> Book,{
    foreignKey: 'bookingUserId'
  })
  public books: HasMany<typeof Book>

  @hasMany(()=> PlayerBook)
  public players: HasMany<typeof PlayerBook>
}
