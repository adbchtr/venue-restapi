import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Field from 'App/Models/Field'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'
export default class FieldsController {
  public async index ({request, response}: HttpContextContract) {
    if (request.qs().name) {
      let name = request.qs().name
      let fieldsFiltered = await Field.findBy("name", name)
      return response.status(200).json({message: 'success get field', data:fieldsFiltered})
    } else {
      let fields = await Field.all()
      return response.status(200).json({message: 'success get field', data:fields})
    }
  }

  public async store ({request, response}: HttpContextContract) {
    try {
      const field = await request.validate(CreateFieldValidator);
      const newField = await Field.create(field)
      return response.created({message: 'created! ', data: newField})
    } catch (error) {
      return response.unprocessableEntity({errors:error.messages})
    }
  }

  public async show ({params, response}: HttpContextContract) {
    // let field = await Field.find(params.id)
    const field = await Field.query().preload('venues').preload('bookings').where('id', params.id).first()
        return response.ok({message: 'success get field with id', data: field})
  }

  public async update ({request, response, params}: HttpContextContract) {
    let id = params.id
        let user = await Field.findOrFail(id)
        user.name = request.input('name')
        user.type = request.input('type')
        user.venueId = request.input('venue_id')
        user.save()
        return response.ok({message: 'updated!'})
  }

  public async destroy ({params, response}: HttpContextContract) {
    let field = await Field.findOrFail(params.id)
        await field.delete()
        return response.ok({message:'deleted!'})
  }
  
  
  
}
